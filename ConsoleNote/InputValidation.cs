﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;

namespace ConsoleNote
{
    static class InputValidation
    {
        //List of all the countries
        private static HashSet<string> countryList = CountryList();

        public static void PrintError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static bool IsValidName(string name)
        {
            if (name.Any(c => Char.IsDigit(c))) return false;
            else return true;
        }

        public static bool IsValidPhoneNumber(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{9,14})$").Success;
        }

        public static bool IsValidCountry(string country)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            return countryList.Contains(cultureInfo.TextInfo.ToTitleCase(country));
        }
        
        // Returns a hashset of all countries
        public static HashSet<string> CountryList()
        {
            HashSet<string> CultureList = new HashSet<string>();

            CultureInfo[] cultureInfos = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            foreach (var culture in cultureInfos)
            {
                RegionInfo regionInfo = new RegionInfo(culture.LCID);
                if (!(CultureList.Contains(regionInfo.EnglishName)))
                {
                    CultureList.Add(regionInfo.EnglishName);
                }   
            }
            return CultureList;
        }
    }
}
