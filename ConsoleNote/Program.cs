﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace ConsoleNote
{
    class Program
    {
        static string fileName = "MyNotes.bin";
        static string fullFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName);

        //NoteCollection instance that holds the list of notes
        static NoteCollection noteCollection = NoteCollection.GetNoteCollectionInstance();

        static void Main(string[] args)
        {
            MenuItems.ShowWelcome();
            Music.PlayMainTheme();
            Console.WriteLine("\nLoading the saved file from {0}", fullFilePath);
            LoadSavedBinaryFile();
            Thread.Sleep(2000);
            Console.Clear();
            StartMainFlow();
            Console.ReadKey();
        }

        // Application's main flow
        static void StartMainFlow()
        {
            // Infinite loop, unless user picks "exit" option
            while (true)
            {
                Console.Clear();
                MenuItems.ShowMenu();
                Console.WriteLine();

                // User input here cannot contain chars
                Console.Write("Your choice - ");
                if (!int.TryParse(Console.ReadLine(), out int choice)) {
                    continue;
                }
                else
                {
                    // if user chooses to exit
                    if (choice == 8)
                    {
                        Music.PlayEndTheme();
                        Console.WriteLine("\nBye Bye!\n");
                        break;
                    }
                    
                    // if user chooses to see the list of notes 
                    else if (choice == 1)
                    {
                        noteCollection.ShowAllNotes();
                    }
                    
                    //if user chooses to show a single note
                    else if (choice == 2)
                    {
                        if (noteCollection.ShowAllNotes())
                        {
                            Console.Write("What note you would like to explore? - ");
                            if (!int.TryParse(Console.ReadLine(), out int noteToShow))
                            {
                                continue;
                            }
                            noteCollection.ShowNote(noteToShow - 1); //indices start at 0
                        }
                    }
                    
                    //if user chooses to create a note
                    else if (choice == 3)
                    {
                        Console.WriteLine();
                        noteCollection.CreateNote();
                    }
                    
                    //if user chooses to edit a note
                    else if (choice == 4)
                    {
                        if (noteCollection.ShowAllNotes())
                        {
                            Console.Write("What note you would like to edit? - ");
                            if (!int.TryParse(Console.ReadLine(), out int noteToEdit))
                            {
                                continue;
                            }
                            noteCollection.EditNote(noteToEdit - 1); //indices start at 0
                        }
                    }

                    //if user chooses to delete a note
                    else if (choice == 5)
                    {
                        if (noteCollection.ShowAllNotes())
                        {
                            Console.Write("What note you would like to delete? - ");
                            if (!int.TryParse(Console.ReadLine(), out int noteToDelete))
                            {
                                continue;
                            }
                            noteCollection.DeleteNote(noteToDelete - 1); //indices start at 0
                        }
                    }

                    //if user chooses to delete all notes
                    else if (choice == 6)
                    {
                        noteCollection.DeleteAllNotes();
                    }

                    //if user chooses to save changes to disk
                    else if (choice == 7)
                    {
                        SaveBinaryFile();
                    }

                    Console.WriteLine("\nPress any key to go to main menu.");
                    Console.ReadKey();
                }
            }
        }

        // Loads the saved file. If no file was found, create new List<note> object
        static void LoadSavedBinaryFile()
        {
            try
            {
                using (Stream stream = File.Open(fullFilePath, FileMode.Open))
                {
                    var bformatter = new BinaryFormatter();
                    noteCollection.MyCollection = (List<Note>)bformatter.Deserialize(stream);
                }
            }
            catch (Exception e)
            {
                //Exception occurred either because a binary is not a List<Note> object
                //or because file does not exist.
                InputValidation.PrintError(e.Message);
                InputValidation.PrintError("\nThe backup file was not found. Creating new...");
                noteCollection.MyCollection = new List<Note>();
            }
        }

        static void SaveBinaryFile()
        {
            try
            {
                using (Stream stream = File.Open(fullFilePath, FileMode.Create))
                {
                    var bformatter = new BinaryFormatter();
                    bformatter.Serialize(stream, noteCollection.MyCollection);
                }
            }
            catch (Exception e)
            {
                InputValidation.PrintError(e.Message);
            } 
            
        }

        //TODO: Implementation is not working yet
        static void LoadSavedXmlFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(fullFilePath))
                {
                    XmlSerializer reader = new XmlSerializer(typeof(List<Note>));
                    noteCollection.MyCollection = (List<Note>)reader.Deserialize(file);
                }    
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                InputValidation.PrintError("\nThe back up file was not found. Creating new...\n");
                noteCollection.MyCollection = new List<Note>();
            }
        }

        //TODO: Implementation is not working yet
        static void SaveXmlFile()
        {
            using (FileStream file = File.Create(fullFilePath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Note>), new XmlRootAttribute("NoteCollection"));
                serializer.Serialize(file, noteCollection.MyCollection);
            }
            
        }



    }
}
