﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.IO;

namespace ConsoleNote
{
    static class Music
    {
        private static SoundPlayer player = new SoundPlayer();

        public static void PlayMainTheme()
        {
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + @"\smb_main.wav";
            player.PlayLooping();
        }
        
        public static void PlayEndTheme()
        {
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + @"\smb_mariodie.wav";
            player.Play();
        }

        public static void PlayExtraSound()
        {
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + @"\smb_stomp.wav";
            player.Play();
        }
    }
}
