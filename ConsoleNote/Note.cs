﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace ConsoleNote
{
    [Serializable]
    public class Note
    {
        public const int TotalFields = 9;

        private string myDateFormat = "dd-MM-yyyy";

        // field number 1
        private string familyName;
        public string FamilyName { get; }

        // field number 2
        private string name;
        public string Name { get; }

        // field number 3
        private string middleName;
        public string MiddleName { get; }

        // field number 4
        private string phoneNumber;
        public string PhoneNumber { get; }

        // field number 5
        private string country;
        public string Country { get; }

        // field number 6
        private DateTime birthdate;
        public DateTime Birthdate { get; }

        // field number 7
        private string organization;
        public string Organization { get; }

        // field nnumber 8
        private string position;
        public string Position { get; }

        // field number 9
        private string extraNotes;
        public string ExtraNotes { get; }


        public string GetSimpleInfo()
        {
            return String.Format("{0}\t{1}\t{2}", familyName, name, phoneNumber);
        }

        public string GetFullInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Family Name:\t {0}\n", familyName);
            sb.AppendFormat("Name:\t\t {0}\n", name);
            sb.AppendFormat("Middle Name:\t {0}\n", middleName);
            sb.AppendFormat("Phone Number:\t {0}\n", phoneNumber);
            sb.AppendFormat("Country:\t {0}\n", country);
            sb.AppendFormat("Birthdate:\t {0}\n", birthdate.Date.ToShortDateString());
            sb.AppendFormat("Organization:\t {0}\n", organization);
            sb.AppendFormat("Position:\t {0}\n", position);
            sb.AppendFormat("Extra notes:\t {0}", extraNotes);
            return sb.ToString();
        }

        public string GetFullInfoWithFieldNumbers()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("[1] Family Name:\t {0}\n", familyName);
            sb.AppendFormat("[2] Name:\t\t {0}\n", name);
            sb.AppendFormat("[3] Middle Name:\t {0}\n", middleName);
            sb.AppendFormat("[4] Phone Number:\t {0}\n", phoneNumber);
            sb.AppendFormat("[5] Country:\t\t {0}\n", country);
            sb.AppendFormat("[6] Birthdate:\t\t {0}\n", birthdate.Date.ToShortDateString());
            sb.AppendFormat("[7] Organization:\t {0}\n", organization);
            sb.AppendFormat("[8] Position:\t\t {0}\n", position);
            sb.AppendFormat("[9] Extra notes:\t {0}", extraNotes);
            return sb.ToString();
        }

        public void EditField(int fieldNumber)
        {
            switch(fieldNumber)
            {
                // string FamilyName - Mandatory
                case 1:
                    EditFamilyName();
                    break;

                // string Name - Mandatory
                case 2:
                    EditName();
                    break;

                //string MiddleName
                case 3:
                    EditMiddleName();
                    break;

                // string PhoneNumber
                case 4:
                    EditPhoneNumber();
                    break;

                // string Country - Mandatory
                case 5:
                    EditCountry();
                    break;

                // DateTime Birthdate
                case 6:
                    EditBirthdate();
                    break;

                // string Organization
                case 7:
                    EditOrganization();
                    break;

                // string Position
                case 8:
                    EditPosition();
                    break;

                // string ExtraNotes
                case 9:
                    EditExtraNotes();
                    break;

                default:
                    InputValidation.PrintError("No such field exists");
                    break;
            }
        }

        private void EditFamilyName()
        {
            while (true)
            {
                Console.Write("FamilyName:\t");
                string userInput = Console.ReadLine();
                if (InputValidation.IsValidName(userInput))
                {
                    familyName = userInput;
                    break;
                }
                else InputValidation.PrintError("\nNames cannot contain numbers!\n");
            }
        }

        private void EditName()
        {
            while (true)
            {
                Console.Write("Name:\t\t");
                string userInput = Console.ReadLine();
                if (InputValidation.IsValidName(userInput))
                {
                    name = userInput;
                    break;
                }
                else InputValidation.PrintError("\nNames cannot contain numbers!\n");
            }
        }

        private void EditMiddleName()
        {
            Console.Write("MiddleName:\t");
            string userInput = Console.ReadLine();
            if (InputValidation.IsValidName(userInput)) middleName = userInput;
            else InputValidation.PrintError("\nNames cannot contain numbers! Skipping...\n");
        }

        private void EditPhoneNumber()
        {
            Console.Write("PhoneNumber:\t");
            string userInput = Console.ReadLine();
            if (InputValidation.IsValidPhoneNumber(userInput)) phoneNumber = userInput;
            else InputValidation.PrintError("\nA valid phone number is 9-14 characters, has + at the start, and contains only digits. Skipping...\n");
        }

        private void EditCountry()
        {
            while (true)
            {
                Console.Write("Country:\t");
                string userInput = Console.ReadLine();
                if (InputValidation.IsValidCountry(userInput))
                {
                    country = userInput;
                    break;
                }
                else InputValidation.PrintError(String.Format("\n{0} is not a valid country.\n", userInput));
            }
        }

        private void EditBirthdate()
        {
            Console.Write("Birthdate\n({0}):\t", myDateFormat);
            if (DateTime.TryParseExact(Console.ReadLine(), myDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out birthdate)) { }
            else
            {
                InputValidation.PrintError("\nInvalid input, skipping...\n");
            }
        }

        private void EditOrganization()
        {
            Console.Write("Organization:\t");
            organization = Console.ReadLine();
        }

        private void EditPosition()
        {
            Console.Write("Position:\t");
            position = Console.ReadLine();
        }

        private void EditExtraNotes()
        {
            Console.Write("ExtraNotes:\t");
            extraNotes = Console.ReadLine();
        }
    }
}
