﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleNote
{
    public class NoteCollection
    {
        private static NoteCollection instance;
        private static string separator = "\n-------------------------------------------\n";

        public List<Note> MyCollection { get; set; }

        public void CreateNewCollection()
        {
            MyCollection = new List<Note>();
        }

        // Private constructor to make it singleton
        private NoteCollection() { }

        // Method to return singleton object
        public static NoteCollection GetNoteCollectionInstance()
        {
            if (instance == null)
            {
                instance = new NoteCollection();
            }
            return instance;
        }

        // Show all notes on the console screen;
        // Return false if empty
        public bool ShowAllNotes()
        {
            if (MyCollection.Count == 0)
            {
                InputValidation.PrintError("\nThe are no notes yet");
                return false;
            }
            else
            {
                Console.WriteLine(separator);
                for (int i = 1; i <= MyCollection.Count; i++)
                {
                    Console.WriteLine("{0} - {1}", i, MyCollection[i - 1].GetSimpleInfo());
                }
                Console.WriteLine(separator);
                return true;
            }
        }

        public void ShowNote(int index)
        {
        
            try
            {
                string output = MyCollection[index].GetFullInfo();
                Console.WriteLine(separator);
                Console.WriteLine(output);
                Console.WriteLine(separator);
            }
            catch (Exception e)
            {
                InputValidation.PrintError(String.Format("\nNo note with the index {0}", index + 1));
            }
            
        }

        // Edit a note with index number
        public void EditNote(int index)
        {
            try
            {
                string output = MyCollection[index].GetFullInfoWithFieldNumbers();
                Console.WriteLine(separator);
                Console.WriteLine(output);
                Console.WriteLine(separator);
                Console.Write("What field would you like to edit? - ");
                
                if (!int.TryParse(Console.ReadLine(), out int fieldToEdit))
                {
                    InputValidation.PrintError("\nInvalid input.");
                    return;
                }
                Console.WriteLine();
                MyCollection[index].EditField(fieldToEdit);
            }
            catch (Exception e)
            {
                InputValidation.PrintError(String.Format("\nNo note with the index {0}", index + 1));
            }
        }

        // Create a new Note object and add it to MyCollection
        public void CreateNote()
        {
            Note note = new Note();
            for (int i = 1; i <= Note.TotalFields; i++)
            {
                note.EditField(i);
            }
            MyCollection.Add(note);
        }

        // Delete a Note with specified index
        public void DeleteNote(int index)
        {
            try
            {
                MyCollection.RemoveAt(index);
            }
            catch (ArgumentOutOfRangeException e)
            {
                InputValidation.PrintError(String.Format("\nNo note with the index {0}", index + 1));
            }
            
        }

        public void DeleteAllNotes()
        {
            MyCollection = new List<Note>();
        }
    }
}
