﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleNote
{
    static class MenuItems
    {
        public static void ShowWelcome()
        {
            string welcomeMessage =
                @"Welcome to the ConsoleNote Application by
   _____  .__    .___.__.__   
  /  _  \ |__| __| _/|__|  |  
 /  /_\  \|  |/ __ | |  |  |  
/    |    \  / /_/ | |  |  |__
\____|__  /__\____ | |__|____/
        \/        \/          ";
            Console.WriteLine(welcomeMessage);
        }

        public static void ShowMenu()
        {
            string menuMessage = @"
1 - Show all notes
2 - Show a note
3 - Create a note
4 - Edit a note
5 - Delete a note
6 - Delete all notes
7 - Save to disk
8 - Exit";
            Console.WriteLine(menuMessage);
        }

    }
}
