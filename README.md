# ConsoleNote Application

This simple console app lets you store your notes.
Notes are saved to Desktop/MyNotes.bin if you choose to save the notes you created during the session.
If not MyNotes.bin is available, it is going to be created. 

## Things that have to be done
* XML Serialization
* User chooses what format and where he wants to save the notes to